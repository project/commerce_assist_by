<?php

namespace Drupal\commerce_assist_by\Controller;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Controller\ControllerBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class AssistByRedirectController.
 */
class AssistByRedirectController extends ControllerBase {

  /**
   * The logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Request.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * WebpayByRedirectController constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   Current logger chanel.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   EntityTypeManager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request.
   */
  public function __construct(LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager, RequestStack $request_stack) {
    $this->logger = $logger;
    $this->entityTypeManager = $entityTypeManager;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory')->get('commerce_assist_by'),
      $container->get('entity_type.manager'),
      $container->get('request_stack')
    );
  }

  /**
   * Complete new payment.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   An order.
   * @param array $data
   *   Data from the request.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function completePayment(OrderInterface $order, array $data) {
    /** @var \Drupal\commerce_payment\Entity\PaymentGateway $payment_gateway */
    $payment_gateway = $order->get('payment_gateway')->first()->entity;
    $payment = $this->entityStorage->create([
      'state' => 'completed',
      'amount' => $order->getTotalPrice(),
      'payment_gateway' => $payment_gateway->id(),
      'order_id' => $order->id(),
      'remote_id' => $data['order_id'],
      'remote_state' => $data['payment_method'],
    ]);
    $payment->save();

    $message = $this->t('New payment for order #@order', ['@order' => $order->id()]);
    $this->logger->info($message, [
      'link' => $order->toLink('Order')->toString(),
    ]);
  }

}
