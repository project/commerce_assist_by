<?php

namespace Drupal\commerce_assist_by\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides the Off-site Redirect payment assist gateway.
 *
 * @CommercePaymentGateway(
 *   id = "assist_by_gateway",
 *   label = "Assist.by (Off-site redirect)",
 *   display_label = "assist.by",
 *   forms = {
 *   "offsite-payment" =
 *   "Drupal\commerce_assist_by\PluginForm\OffsiteRedirect\AssistByRedirectForm"
 *   },
 * )
 */
class AssistByPaymentGateway extends OffsitePaymentGatewayBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $defaults = [
      'commerce_assist_by_shop_id' => '',
      'commerce_assist_by_shop_secret' => '',
      'commerce_assist_by_server_name' => '',
      'commerce_assist_by_currency_id' => '',
      'commerce_assist_by_language_id' => '',
      'commerce_assist_by_return_ok_url' => '',
      'commerce_assist_by_return_no_url' => '',
    ];

    return $defaults + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['commerce_assist_by_shop_id'] = [
      '#title' => $this->t('Shop id'),
      '#description' => $this->t('Your Assist shop id.'),
      '#default_value' => $this->configuration['commerce_assist_by_shop_id'],
      '#type' => 'textfield',
      '#required' => TRUE,
    ];

    $form['commerce_assist_by_shop_secret'] = [
      '#title' => $this->t('Secret key'),
      '#description' => $this->t('Assist shop secret key'),
      '#default_value' => $this->configuration['commerce_assist_by_shop_secret'],
      '#type' => 'textfield',
      '#required' => TRUE,
    ];

    $form['commerce_assist_by_server_name'] = [
      '#title' => $this->t('Server name'),
      '#description' => $this->t('Assist server name'),
      '#default_value' => $this->configuration['commerce_assist_by_server_name'],
      '#type' => 'textfield',
    ];

    $form['commerce_assist_by_currency_id'] = [
      '#title' => $this->t('OrderCurrency'),
      '#description' => $this->t('Supported currency identifier (BYN, USD, EUR, RUB).'),
      '#default_value' => $this->configuration['commerce_assist_by_currency_id'] ?: 'BYN',
      '#type' => 'select',
      '#options' => $this->availableCurrencies(),
      '#required' => TRUE,
    ];

    $form['commerce_assist_by_language_id'] = [
      '#title' => $this->t('Language'),
      '#description' => $this->t('Supported values are "russian" or "english".'),
      '#default_value' => $this->configuration['commerce_assist_by_language_id'] ?: 0,
      '#type' => 'select',
      '#options' => $this->availableLanguages(),
      '#required' => TRUE,
    ];

    $form['commerce_assist_by_return_ok_url'] = [
      '#title' => $this->t('URL_RETURN_OK'),
      '#description' => $this->t('URL страницы, куда должен вернуться покупатель после успешного осуществления платежа в системе АПК Ассист'),
      '#default_value' => $this->configuration['commerce_assist_by_return_ok_url'],
      '#type' => 'textfield',
    ];

    $form['commerce_assist_by_return_no_url'] = [
      '#title' => $this->t('URL_RETURN_NO'),
      '#description' => $this->t('URL страницы, куда должен вернуться покупатель после неуспешного осуществления платежа в системе АПК Ассист или при отсутствии ответа об окончательном статусе платежа'),
      '#default_value' => $this->configuration['commerce_assist_by_return_no_url'],
      '#type' => 'textfield',
    ];

    return $form;
  }

  /**
   * Currencies supported by API.
   *
   * @return array
   *   Currencies
   */
  public function availableCurrencies() {
    return [
      'BYN' => $this->t('BYN'),
      'USD' => $this->t('USD'),
      'EUR' => $this->t('EUR'),
      'RUB' => $this->t('RUB'),
    ];
  }

  /**
   * Language list supported by API.
   *
   * @return array
   *   Languages
   */
  public function availableLanguages() {
    return [
      'RU' => $this->t('Russian'),
      'EN' => $this->t('English'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $required_fields = [
      'commerce_assist_by_shop_id',
      'commerce_assist_by_server_name',
    ];

    foreach ($required_fields as $key) {
      if (empty($values[$key])) {
        $message = $this->t('Service is not configured for use. Please contact an administrator to resolve this issue.');
        $this->messenger()->addError($message);
        return FALSE;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['commerce_assist_by_shop_id'] = $values['commerce_assist_by_shop_id'];
      $this->configuration['commerce_assist_by_shop_secret'] = $values['commerce_assist_by_shop_secret'];
      $this->configuration['commerce_assist_by_server_name'] = $values['commerce_assist_by_server_name'];
      $this->configuration['commerce_assist_by_currency_id'] = $values['commerce_assist_by_currency_id'];
      $this->configuration['commerce_assist_by_return_ok_url'] = $values['commerce_assist_by_return_ok_url'];
      $this->configuration['commerce_assist_by_return_no_url'] = $values['commerce_assist_by_return_no_url'];
    }
  }

}
