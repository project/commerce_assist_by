<?php

namespace Drupal\commerce_assist_by\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Utility\Token;

/**
 * Provides the class for payment off-site form.
 *
 * Provide a buildConfigurationForm() method which calls buildRedirectForm()
 * with the right parameters.
 */
class AssistByRedirectForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  const PRECISION = 2;
  const DEC_POINT = '.';
  const THOUSANDS_SEP = '';
  const ROUND_MODE = PHP_ROUND_HALF_UP;

  /**
   * Gateway plugin.
   *
   * @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface
   */
  private $paymentGatewayPlugin;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ModuleHandlerInterface $module_handler
   *   The handle of module objects.
   * @param \Drupal\Core\Utility\Token $token
   *   The token utility.
   */
  public function __construct(ModuleHandlerInterface $module_handler, Token $token) {
    $this->moduleHandler = $module_handler;
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('token')
    );
  }

  /**
   * Getting plugin's configuration.
   *
   * @param string $configuration
   *   Configuration name.
   *
   * @return mixed
   *   Configuration value.
   */
  private function getConfiguration($configuration) {
    return $this->paymentGatewayPlugin->getConfiguration()[$configuration] ?? NULL;
  }

  /**
   * Round price.
   *
   * @param float $number
   *   Price.
   * @param int $precision
   *   Precision.
   * @param int $mode
   *   Round mode.
   *
   * @return float
   *   Rounded price.
   */
  private function round(float $number, $precision = self::PRECISION, $mode = self::ROUND_MODE) {
    return (float) number_format(round($number, $precision, $mode), $precision, self::DEC_POINT, self::THOUSANDS_SEP);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $this->paymentGatewayPlugin = $payment->getPaymentGateway()->getPlugin();

    $order = $payment->getOrder();

    $redirect_url = $this->getConfiguration('commerce_assist_by_server_name') . '/pay/order.cfm';
    $total_price = 0;
    $shipping_price = 0;
    $data = [];

    $data['Merchant_ID'] = $this->getConfiguration('commerce_assist_by_shop_id');
    $data['OrderNumber'] = $payment->getOrderId();
    $data['OrderCurrency'] = $this->getConfiguration('commerce_assist_by_currency_id');
    $data['OrderComment'] = '';

    if ($return_ok_url = $this->getConfiguration('commerce_assist_by_return_ok_url')) {
      $data['URL_RETURN_OK'] = $this->token->replace($return_ok_url, ['commerce_order' => $order]);
    }
    if ($return_no_url = $this->getConfiguration('commerce_assist_by_return_no_url')) {
      $data['URL_RETURN_NO'] = $this->token->replace($return_no_url, ['commerce_order' => $order]);
    }

    $data['Email'] = $order->getEmail();
    $data['FirstName'] = $order->getBillingProfile()->get('address')->getValue()[0]['given_name'];
    $data['LastName'] = $order->getBillingProfile()->get('address')->getValue()[0]['family_name'];
    $data['Zip'] = $order->getBillingProfile()->get('address')->getValue()[0]['postal_code'];
    $data['Address'] = $order->getBillingProfile()->get('address')->getValue()[0]['address_line1'];
    $data['City'] = $order->getBillingProfile()->get('address')->getValue()[0]['locality'];

    /** @var \Drupal\commerce_order\Entity\OrderItem $item */
    foreach ($order->getItems() as $item) {
      $price_per_item = $this->round($item->getUnitPrice()->getNumber());
      $quantity = round($item->getQuantity(), 0);
      $total_price += $price_per_item * $quantity;
    }

    // /** @var \Drupal\commerce_order\Adjustment $item */
    foreach ($order->getAdjustments() as $item) {
      if ($item->getType() === 'shipping') {
        $shipping_price += $this->round($item->getAmount()->getNumber());
      }
    }

    $subtotalPrice = $order->getSubtotalPrice()->getNumber();
    $totalPrice = $order->getTotalPrice()->getNumber();
    $discount = $this->round($subtotalPrice - $totalPrice);

    $shippingPrice = $total_price + $this->round($shipping_price);
    $discountPrice = $discount + $this->round($shipping_price);

    $data['OrderAmount'] = $this->round($shippingPrice - $discountPrice);

    $prepare_salt = [
      $data['Merchant_ID'],
      $data['OrderNumber'],
      $data['OrderAmount'],
      $data['OrderCurrency'],
    ];
    $salt = implode(';', $prepare_salt);
    $x = $this->getConfiguration('commerce_assist_by_shop_secret');

    $data['Checkvalue'] = mb_strtoupper(md5(mb_strtoupper(md5($x) . md5($salt))));
    $data['Language'] = 'RU';
    $data['CardPayment'] = 1;

    // Call modules that implement the hook, and let them add items.
    $this->moduleHandler->alter('commerce_assist_by', $data, $order);

    return $this->buildRedirectForm($form, $form_state, $redirect_url, $data, self::REDIRECT_POST);
  }

}
