# Drupal 8 Commerce 2 module: Commerce Assist.by Payment gateway
----------------------------------------------------------------

CONTENTS OF THIS FILE
---------------------

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers


INTRODUCTION
------------

This module implements a Drupal Commerce payment method, to embed the payment
services provided by [Assist.by](https://assist.by/) payment gateway

Module also supports tokens, to configure success / canceled urls. And choosing
currency supported by gateway (RUB, USD, EUR).

For a full description of the project visit the project page:
https://www.drupal.org/project/commerce_assist_by

To submit bug reports and feature suggestions, or to track changes:
http://drupal.org/project/issues/commerce_assist_by


REQUIREMENTS
------------

 * Commerce Payment (from [Commerce](http://drupal.org/project/commerce) core)
 * Commerce Order (from [Commerce](http://drupal.org/project/commerce) core)
 * [Token](http://drupal.org/project/token)


INSTALLATION
------------

**By the composer (recommended)**:
```bash
composer require drupal/commerce_assist_by
```

**Manually**:

Install the Commerce Webpay.by Payment module, as usual, by copying the sources
to a modules directory, such as `modules/contrib` or `modules`. 

See: https://drupal.org/documentation/install/modules-themes/modules-8 for
further information.


CONFIGURATION
-------------

- In your Drupal site, enable the module.
- Go to `admin/commerce/config/payment-gateways` 
(*Commerce* -> *Configuration* -> *Payment gateways*), and add a new payment
method.
- Configure your API keys


MAINTAINERS
-----------

Current maintainers:
- Alex Domasevich - https://www.drupal.org/u/alexdoma
